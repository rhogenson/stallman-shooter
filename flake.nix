{
  description = "An RMS-themed bullet hell game.";

  outputs = { self, nixpkgs }:
  let pkgs = nixpkgs.legacyPackages.x86_64-linux; in
  rec {

    packages.x86_64-linux.stallman-shooter = pkgs.rustPlatform.buildRustPackage {
      pname = "stallman-shooter";
      version = "1.0.0";

      src = self;

      cargoLock = {
        lockFile = ./Cargo.lock;
      };

      nativeBuildInputs = with pkgs; [
        clippy
      ];

      buildInputs = with pkgs; [
        SDL2
        SDL2_image
      ];
    };

    defaultPackage.x86_64-linux = packages.x86_64-linux.stallman-shooter;

    checks.build = packages.x86_64-linux.stallman-shooter;
  };
}
