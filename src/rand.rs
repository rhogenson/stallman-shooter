use std::sync::Mutex;
use std::time::SystemTime;

static STATE: Mutex<u64> = Mutex::new(0);

pub fn seed() {
    let n = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap();
    *STATE.lock().unwrap() = n.as_nanos() as u64;
}

fn mix(s: &mut u64) {
    // https://en.wikipedia.org/wiki/Linear_congruential_generator
    *s = s.wrapping_mul(6364136223846793005).wrapping_add(1);
}

pub fn random() -> f64 {
    let mut s = STATE.lock().unwrap();
    mix(&mut s);
    (*s >> 32) as f64 / 4294967296.
}
