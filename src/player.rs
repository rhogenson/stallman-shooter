use super::constants;
use super::state;

pub struct Player {
    pub x: i32,
    pub y: i32,
    left_p: bool,
    right_p: bool,
    up_p: bool,
    down_p: bool,
    shift_p: bool,
    y_d: i32,
    x_d: i32,
    pub fire_rate: u32,
}

impl Player {
    pub fn new() -> Player {
        Player {
            x: 400,
            y: 500,
            x_d: 0,
            y_d: 0,
            down_p: false,
            left_p: false,
            right_p: false,
            up_p: false,
            fire_rate: 15,
            shift_p: false,
        }
    }

    pub fn act(&mut self, events: &mut sdl2::event::EventPollIterator) -> Result<(), ()> {
        for event in events {
            match event {
                sdl2::event::Event::Quit { .. } => {
                    return Err(());
                }
                sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::Down),
                    ..
                } => {
                    self.down_p = true;
                    self.y_d = -1;
                }
                sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::Down),
                    ..
                } => {
                    self.down_p = false;
                }
                sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::Up),
                    ..
                } => {
                    self.up_p = true;
                    self.y_d = 1;
                }
                sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::Up),
                    ..
                } => {
                    self.up_p = false;
                }
                sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::Left),
                    ..
                } => {
                    self.left_p = true;
                    self.x_d = -1;
                }
                sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::Left),
                    ..
                } => {
                    self.left_p = false;
                }
                sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::Right),
                    ..
                } => {
                    self.right_p = true;
                    self.x_d = 1;
                }
                sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::Right),
                    ..
                } => {
                    self.right_p = false;
                }
                sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::LShift),
                    ..
                }
                | sdl2::event::Event::KeyDown {
                    keycode: Some(sdl2::keyboard::Keycode::RShift),
                    ..
                } => {
                    self.shift_p = true;
                }
                sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::LShift),
                    ..
                }
                | sdl2::event::Event::KeyUp {
                    keycode: Some(sdl2::keyboard::Keycode::RShift),
                    ..
                } => {
                    self.shift_p = false;
                }
                _ => {}
            }
        }
        if self.left_p {
            if !self.right_p {
                self.x_d = -1;
            }
        } else if self.right_p {
            self.x_d = 1;
        } else {
            self.x_d = 0;
        }
        if self.up_p {
            if !self.down_p {
                self.y_d = -1;
            }
        } else if self.down_p {
            self.y_d = 1;
        } else {
            self.y_d = 0;
        }
        /* Don't go off the side */
        if self.x_d < 0 && self.x <= 0 || self.x >= constants::X_DIM as i32 && self.x_d > 0 {
            self.x_d = 0;
        }
        if self.y <= 0 && self.y_d < 0 || self.y >= constants::Y_DIM as i32 && self.y_d > 0 {
            self.y_d = 0;
        }

        self.x += if self.shift_p { self.x_d } else { self.x_d * 3 };
        self.y += if self.shift_p { self.y_d } else { self.y_d * 3 };
        Ok(())
    }
}

impl state::Positioned for Player {
    fn x(&self) -> i32 {
        self.x
    }
    fn y(&self) -> i32 {
        self.y
    }
    fn radius() -> i32 {
        3
    }
}

impl state::Drawable for Player {
    fn center() -> (u32, u32) {
        (33, 21)
    }
}
